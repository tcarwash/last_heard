from app import db
from datetime import datetime

class Spot(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    callsign = db.Column(db.String(64), index=True, unique=True)
    frequency = db.Column(db.String(64), index=True)
    mode = db.Column(db.String(64), index=True)
    time = db.Column(db.DateTime(64), default=datetime.utcnow, index=True)

    def __repr__(self):
        return '<callsign: {}>, mode: {}, time: {}'.format(self.callsign, self.mode, self.time)
