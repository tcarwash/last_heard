from flask import jsonify, request, render_template, flash, redirect, url_for
from app import app, db
from app.models import Spot
from datetime import datetime


@app.route('/data', methods=['POST'])
def intake_data():
    if request.method == 'POST' and request.form['dxcc']:
        record = Spot.query.filter_by(callsign=request.form['callsign'].upper()).first()
        if not record:
            record = Spot(callsign=request.form['callsign'].upper(), mode=request.form['mode'].upper(), frequency=request.form['frequency'])
        else:
            record.mode = request.form['mode'].upper()
            record.frequency = request.form['frequency']
            record.time = datetime.utcnow()
        db.session.add(record)
        db.session.commit()
        resp = jsonify("Slurp, Thanks!")
        resp.status_code = 200
    else:
        resp = jsonify('Not accepted!')
        resp.status_code = 401
    return resp


@app.route('/data/<count>', methods=['GET'])
@app.route('/data', methods=['GET'])
def output_all(count=None):
    if count:
        data = Spot.query.order_by(Spot.time.desc()).limit(int(count))
    else:
        data = Spot.query.order_by(Spot.time.desc()).all()
    data = [{'callsign': item.callsign, 'mode': item.mode, 'frequency': item.frequency, 'time': item.time} for item in data]
    response = jsonify(data)

    return response


@app.route('/last_heard/<callsign>', methods=['GET'])
@app.route('/last_heard', methods=['GET'])
def ouput_call(callsign=None):
    if callsign:
        record = Spot.query.filter_by(callsign=callsign.upper()).first()
    else:
        record = Spot.query.first()
    if record:
        response = jsonify({'callsign': record.callsign,
                            'mode': record.mode,
                            'frequency': record.frequency,
                            'time': record.time})
    elif callsign:
        response = jsonify("{0} de AG7SU... {0} de AG7SU... Nothing Heard!".format(callsign.upper()))
    else:
        response = jsonify("No data found!")

    return response

@app.route('/', methods=['GET'])
def index():
    data = Spot.query.order_by(Spot.time.desc()).limit(5)
    return render_template('index.html', title='Last Heard', data=data)