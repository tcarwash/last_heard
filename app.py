from app import app, db
from app.models import Spot
from waitress import serve

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Spot': Spot}

if __name__ == "__main__":
    serve(app, host='0.0.0.0', port=5000)